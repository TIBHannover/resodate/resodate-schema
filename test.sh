#!/bin/sh

set -e

run_ajv() {
  ajv test -s schemas/metadata/schema.json -r "schemas/metadata/!(schema).json" \
    -d "$1" -c ajv-formats $2
}

run_ajv "examples/valid/*.json" --valid
run_ajv "examples/invalid/*.json" --invalid

echo "All tests \033[0;32mPASSED\033[0m\n"
